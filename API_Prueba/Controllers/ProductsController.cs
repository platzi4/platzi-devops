﻿using API_Prueba.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace API_Prueba.Controllers
{
   public class ProductsController : ApiController
   {
      /// <summary>
      /// List of bank products
      /// </summary>
      Product[] products = new Product[]
      {
            new Product { Id = 1, Name = "Daviplata", Category = "Debito", Price = 500000 },
            new Product { Id = 2, Name = "Cuenta Ahorros", Category = "Debito", Price = 5000000 },
            new Product { Id = 3, Name = "Credito Vivienda", Category = "Credito", Price = 80000000 }
      };

      /// <summary>
      ///It returns all the products available of the bank
      /// </summary>
      /// <returns></returns>
      public IEnumerable<Product> GetAllProducts()
      {
         return products;
      }

      /// <summary>
      /// It returns a specific product available of the bank
      /// </summary>
      /// <param name="id">Identification of the product</param>
      /// <returns></returns>
      public IHttpActionResult GetProduct(int id)
      {
         var product = products.FirstOrDefault((p) => p.Id == id);
         if (product == null)
         {
            return NotFound();
         }
         return Ok(product);
      }
   }
}